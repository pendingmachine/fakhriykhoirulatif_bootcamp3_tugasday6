import axios from "axios";

const APIRequest = axios.create({
  baseURL: "http://localhost:3004",
  timeout: 10000,
});

export default APIRequest;
