import React from "react";

import AppBar from "@mui/material/AppBar";
import IconButton from "@mui/material/IconButton";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";

import CameraAltIcon from "@mui/icons-material/CameraAlt";
import MenuIcon from "@mui/icons-material/Menu";

const Navbar = () => {
  return (
    <AppBar position="static">
      <Toolbar
        sx={{
          display: "flex",
          gap: "16px",
        }}
      >
        <IconButton sx={{ color: "white" }}>
          <CameraAltIcon />
        </IconButton>
        <Typography
          variant="h6"
          component="div"
          sx={{ display: { xs: "none", sm: "block" } }}
        >
          Blog App
        </Typography>
        <Typography
          variant="h6"
          component="div"
          sx={{ display: { xs: "none", sm: "block" } }}
        >
          About
        </Typography>
        <Typography
          variant="h6"
          component="div"
          sx={{ display: { xs: "none", sm: "block" } }}
        >
          Memory
        </Typography>
        <Typography
          variant="h6"
          component="div"
          sx={{ display: { xs: "none", sm: "block" } }}
          ml="auto"
        >
          Login
        </Typography>
        <IconButton
          sx={{
            color: "white",
            marginLeft: "auto",
            display: { xs: "block", sm: "none" },
          }}
        >
          <MenuIcon />
        </IconButton>
      </Toolbar>
    </AppBar>
  );
};

export default Navbar;
