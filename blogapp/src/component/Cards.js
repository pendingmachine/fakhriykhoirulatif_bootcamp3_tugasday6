import React, { useState } from "react";

import Avatar from "@mui/material/Avatar";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardHeader from "@mui/material/CardHeader";
import CardMedia from "@mui/material/CardMedia";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";

import { getPost } from "../api/main/post";

function Cards() {
  const [posts, setPosts] = useState([]);

  if (posts.length == 0) {
    getPost().then((data) => setPosts(data));
  }
  console.log(posts);

  return (
    <Grid
      container
      spacing={2}
      sx={{
        border: { xs: "none", sm: "8px solid pink" },
        marginTop: "0",
        paddingRight: "16px",
      }}
    >
      {posts.map((post) => (
        <Grid item xs={12} sm={6}>
          <Card>
            <CardHeader
              avatar={<Avatar sx={{ backgroundColor: "red" }}>R</Avatar>}
              title={post.title}
              subheader={post.datePost}
            />
            <CardMedia component="img" image={post.img} />
            <CardContent>
              <Typography variant="body2" color="text.secondary">
                {post.description}
              </Typography>
            </CardContent>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
}

export default Cards;
