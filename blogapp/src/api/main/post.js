import axios from "../axios";

function getPost() {
  return axios.get("postgenerated").then((response) => response.data);
}

export { getPost };
